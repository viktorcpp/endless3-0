
#include "esPhysics.h"

namespace endless
{
    namespace esphysics
    {
        OVERRIDE void esPhysics::Update()
        {}

        OVERRIDE void esPhysics::Setup() throw(std::exception)
        {}

        esPhysicsAPI* esPhysicsAPI::Create()
        {
            return new esPhysics();

        } // Create

        esPhysics::esPhysics()
        {
            //esUtils::alert("Hello from esPhysics !");
        }

        esPhysics::~esPhysics()
        {}

    } // namespace esphysics

} // namespace endless

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
    return TRUE;

} // DllMain
