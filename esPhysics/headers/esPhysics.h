#pragma once

#include "esPhysicsAPI.h"

namespace endless
{
    namespace esphysics
    {
        class ES_PHYSICS_API esPhysics : public esPhysicsAPI
        {
        public:

            OVERRIDE void Update();

            OVERRIDE void Setup() throw(std::exception);

            esPhysics();
            ~esPhysics();

        private:

            esPhysics( esPhysics& ) = delete;

        }; // class esPhysics

    } // namespace esphysics

} // namespace endless

