#pragma once

#include "esShared/defines.h"
#include "esShared/includes-std.h"
#include "esShared/esUtils.h"

#ifdef ES_PHYSICS_EXPORT
#define ES_PHYSICS_API __declspec(dllexport)
#else
#define ES_PHYSICS_API __declspec(dllimport)
#endif

namespace endless
{
    namespace esphysics
    {
        class ES_PHYSICS_API esPhysicsAPI
        {
        public:

            virtual void Update() = 0;

            static esPhysicsAPI* Create();

            virtual void Setup() throw(std::exception) = 0;

        }; // class esPhysicsAPI

    } // namespace esphysics

} // namespace endless
