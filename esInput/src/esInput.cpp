
#include "esInput.h"

namespace endless
{
    namespace esinput
    {
        OVERRIDE void esInput::Update()
        {
            //

        } // Update

        OVERRIDE void esInput::Reset()
        {
            //

        } // Reset

        OVERRIDE void esInput::ResetUpKeys()
        {
            //

        } // ResetUpKeys

        OVERRIDE void esInput::ReadM(LPARAM)
        {
            //

        } // ReadM

        void esInput::Setup( HWND hwnd ) throw(std::exception)
        {
            hWnd = hwnd;

        } // Setup

        esInput::esInput()
        {
            //esUtils::alert("Hello from esInput !");
        }

        esInput::~esInput()
        {}

        esInputAPI* esInputAPI::Create()
        {
            return new esInput();

        } // Create

    } // namespace esinput

} // namespace endless

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
    return TRUE;

} // DllMain
