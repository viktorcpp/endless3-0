#pragma once

#include "esInputAPI.h"

namespace endless
{
    namespace esinput
    {
        class ES_INPUT_API esInput : public esInputAPI
        {
        public:

            OVERRIDE void Update();
            OVERRIDE void Reset();
            OVERRIDE void ResetUpKeys();
            OVERRIDE void ReadM(LPARAM);

            OVERRIDE void Setup( HWND hwnd ) throw(std::exception);

            esInput();
            ~esInput();

        private:

             HWND hWnd = nullptr;

            esInput( esInput& ) = delete;

        }; // class esInput

    } // namespace esinput

} // namespace endless

