#pragma once

#include "esShared/defines.h"
#include "esShared/includes-std.h"
#include "esShared/esUtils.h"

#ifdef ES_INPUT_EXPORT
#define ES_INPUT_API __declspec(dllexport)
#else
#define ES_INPUT_API __declspec(dllimport)
#endif

namespace endless
{
    namespace esinput
    {
        INTERFACE class ES_INPUT_API esInputAPI
        {
        public:

            virtual void Update() = 0;
            virtual void Reset() = 0;
            virtual void ResetUpKeys() = 0;
            virtual void ReadM(LPARAM) = 0;

            static esInputAPI* Create();

            virtual void Setup( HWND hwnd ) throw(std::exception) = 0;

        }; // class esInputAPI

    } // namespace esinput

} // namespace endless
