
#include "main.h"
#include "Scripts.h"
#include "Scripts.cpp"

int main()
{
    JsErrorCode js_error_code = JsNoError;
    Scripts     scripts;

    scripts.Setup();
    scripts.RegisterFunc();

    //================ test script exec

    std::stringstream script_string;
    endless::esUtils::FileLoadBuffer( "test-base/main.script", script_string );
    
    js_error_code = scripts.DoString( script_string.str().c_str() );

    if( js_error_code != JsNoError )
    {
        scripts.TranslateLastError();
    }

    std::string str_test;
    scripts.GetValueString(str_test);

    return EXIT_SUCCESS;

} // main
