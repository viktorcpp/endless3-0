#pragma once

class Scripts
{
public:

    struct module_job
    {
	    JsModuleRecord  module_record;
	    char*           source;
	    JsSourceContext source_context;
	    size_t          source_size;
    };

    void GetValueString(std::string& name);

    JsErrorCode DoString( const char* script );

    bool Setup();
    bool RegisterFunc();
    void TranslateLastError();

    static JsValueRef CALLBACK ConsoleLog(JsValueRef callee, bool isConstructCall, JsValueRef* arguments, unsigned short arguments_count, void* callbackState);

    Scripts();
    virtual ~Scripts();

private:

    JsRuntimeHandle runtime;
	JsContextRef    context;
	JsValueRef      result;
	unsigned int    currentSourceContext = 0;

    Scripts(Scripts&);

}; // class Scripts
