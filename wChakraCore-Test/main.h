#pragma once

#include <windows.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

#define _JSRT_
#include "ChakraCore.h"

#ifdef _DEBUG
#pragma comment(lib, "ChakraCore.lib")
#else
#pragma comment(lib, "ChakraCore.lib")
#endif

#include "esUtils.h"
