
JsErrorCode Scripts::DoString( const char* script_string )
{
    JsErrorCode js_error_code = JsNoError;

    std::string  script_string_mb(script_string);
    std::wstring script_string_wide( script_string_mb.begin(), script_string_mb.end() );

    js_error_code = JsRunScript( script_string_wide.c_str(), currentSourceContext++, L"", NULL );

    // Convert your script result to String in JavaScript; redundant if your script returns a String

    /*/js_error_code = JsParseScript( script_string_wide.c_str(), currentSourceContext++, L"", &result );

	JsValueRef resultJSString;
	JsConvertValueToString(result, &resultJSString);

    // Project script result in JS back to C++.
	const wchar_t* resultWC;
	size_t         stringLength;
	JsStringToPointer(resultJSString, &resultWC, &stringLength);

    std::wstring resultW(resultWC);
	std::cout << "\nEND: " << std::string(resultW.begin(), resultW.end()) << std::endl;
    //*/

    //JsCallFunction( _In_ JsValueRef function, _In_reads_(argumentCount) JsValueRef *arguments, _In_ unsigned short argumentCount, _Out_opt_ JsValueRef *result);
    //JsConvertValueToBoolean
    //JsConvertValueToNumber
    //JsConvertValueToObject
    //JsConvertValueToString
    //JsDoubleToNumber
    //JsIntToNumber
    //JsCreateArray
    //JsPointerToString

    //JsGetOwnPropertyNames(_In_ JsValueRef object,_Out_ JsValueRef *propertyNames);
    //JsObjectGetOwnPropertyDescriptor( _In_ JsValueRef object, _In_ JsValueRef key, _Out_ JsValueRef *propertyDescriptor);
    //JsHasException( _Out_ bool *hasException);
    //JsObjectGetProperty( _In_ JsValueRef object, _In_ JsValueRef key, _Out_ JsValueRef *value );
    //JsNumberToDouble( _In_ JsValueRef value, _Out_ double *doubleValue);
    //JsNumberToInt( _In_ JsValueRef value, _Out_ int *intValue );
    //JsParseScript( _In_z_ const wchar_t *script, _In_ JsSourceContext sourceContext, _In_z_ const wchar_t *sourceUrl, _Out_ JsValueRef *result);

    return js_error_code;

} // DoString

void Scripts::GetValueString(std::string& name)
{
    JsValueRef global_object;
    JsGetGlobalObject(&global_object);

    JsPropertyIdRef prop_id_ref;
    JsGetPropertyIdFromName(L"TestVar", &prop_id_ref);

    JsValueRef prop_value_ref;
    JsGetProperty(global_object, prop_id_ref, &prop_value_ref);

    const wchar_t* str;
    size_t str_len;
    JsStringToPointer(prop_value_ref, &str, &str_len);

    std::wstring str_w( str );

    std::cout << "\nTEST: " << std::string( str_w.begin(), str_w.end() ) << std::endl;

    /*/ TEST
    std::cout << "\nTEST for Getting Property ==========================================\n";
    JsErrorCode error_code;
    JsPropertyIdRef prop_id_obj_ref;
    error_code = JsGetPropertyIdFromName(L"CFG", &prop_id_obj_ref);
    error_code = JsGetProperty(global_object, prop_id_obj_ref, &prop_value_ref);
    JsValueType prop_value_type; // == JsObject
    error_code = JsGetValueType( prop_value_ref, &prop_value_type );
    JsValueRef property_names;
    error_code = JsGetOwnPropertyNames( prop_value_ref, &property_names ); // */


    JsErrorCode     error_code;
    JsPropertyIdRef function_id_ref;
    JsValueRef      function_value_ref;
    JsValueRef      function_result;
    std::wstring    str_arg0( L"CFG['test_var1']" );
    JsValueRef      str_arg_value;
    JsValueRef      args[2];
    JsPointerToString( str_arg0.c_str(), str_arg0.size(), &str_arg_value);
    args[1] = str_arg_value;

    JsGetPropertyIdFromName(L"GetProperty", &function_id_ref);
    JsGetProperty(global_object, function_id_ref, &function_value_ref);
    error_code = JsCallFunction( function_value_ref, args, 2, &function_result);
    if( error_code != JsNoError )
    {
        std::cout << "SUKA BLYAT" << std::endl;
    }

    JsValueRef resultJSString;
	JsConvertValueToString(function_result, &resultJSString);

    error_code = JsStringToPointer(resultJSString, &str, &str_len);
    if( error_code != JsNoError )
    {
        std::cout << "SUKA BLYAT " << (int)error_code << std::endl;
    }
    std::wstring str_out(str);

    std::cout << "JsCallFunction returns = " << std::string( str_out.begin(), str_out.end() ) << std::endl;

} // GetValueString

void Scripts::TranslateLastError()
{
    JsValueRef exception;
	JsGetAndClearException(&exception);

    JsPropertyIdRef messageName;
	JsGetPropertyIdFromName(L"message", &messageName);

    JsValueRef messageValue;
	JsGetProperty(exception, messageName, &messageValue);

    const wchar_t* message;
	size_t length;
	JsStringToPointer(messageValue, &message, &length);
    std::string message_s;
    endless::esUtils::WtoL( message, message_s);
    std::cout << std::endl << "Script FATAL error: " << message_s << std::endl;

} // TranslateLastError

bool Scripts::Setup()
{
    // Create a runtime. 
	JsCreateRuntime(JsRuntimeAttributeEnableExperimentalFeatures, nullptr, &runtime);
    // Create an execution context. 
	JsCreateContext(runtime, &context);
    // Now set the execution context as being the current one on this thread.
	JsSetCurrentContext(context);

    return true;

} // Setup

bool Scripts::RegisterFunc()
{
    // Reg console.log(string)
    JsValueRef      console, log_func, global;
    JsPropertyIdRef console_prop_id, log_prop_id;
    const char*     logString     = "log";
    const char*     consoleString = "console";

    JsCreateObject(&console);
    JsCreateFunction(Scripts::ConsoleLog, nullptr, &log_func);
    JsCreatePropertyId(logString, strlen(logString), &log_prop_id);
    JsSetProperty(console, log_prop_id, log_func, true);
    JsGetGlobalObject(&global);
    JsCreatePropertyId(consoleString, strlen(consoleString), &console_prop_id);
    JsSetProperty(global, console_prop_id, console, true);

    return true;

} // RegisterFunc

JsValueRef CALLBACK Scripts::ConsoleLog(JsValueRef callee, bool isConstructCall, JsValueRef* arguments, unsigned short arguments_count, void* callbackState)
{
    std::stringstream out;
    for( unsigned int index = 1; index < arguments_count; index++ )
    {
        if( index > 1 ) std::cout << "    ";

        JsValueRef     string_value_ref = nullptr;
        const wchar_t* result_wc        = nullptr;
	    size_t         string_length    = 0;

        JsConvertValueToString( arguments[index], &string_value_ref );
	    JsStringToPointer( string_value_ref, &result_wc, &string_length );

        std::wstring result_string(result_wc);
	    out << std::string( result_string.begin(), result_string.end() );
    }

    std::cout << std::endl << out.str() << std::endl;

    return JS_INVALID_REFERENCE;

} // ConsoleLog

Scripts::Scripts()
{}

Scripts::Scripts(Scripts&)
{}

Scripts::~Scripts()
{
    // Dispose runtime
	JsSetCurrentContext(JS_INVALID_REFERENCE);
	JsDisposeRuntime(runtime);
}
