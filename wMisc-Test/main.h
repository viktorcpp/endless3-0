#pragma once

#include <includes-std.h>

#include "esUtils.h"

namespace endless
{
    void Parse( _In_ std::string& data, _Out_ std::map<std::string, std::string>& result);

} // namespace endless
