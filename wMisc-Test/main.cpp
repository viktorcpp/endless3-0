
#include "main.h"
#include <math.h>
#include <xmmintrin.h>

using namespace endless;

int main()
{
    for( int playerLevel = 1; playerLevel < 201; playerLevel++ )
    {
        __declspec(align(16)) __m128 a = { 300.0, 4.0, 4.0, 12.0 };
        __declspec(align(16)) __m128 b = {   1.5, 2.5, 3.5,  4.5 };

        _mm_mul_ss(a, b);

    }

} // main
