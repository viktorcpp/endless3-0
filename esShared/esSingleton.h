#pragma once

#pragma once

namespace endless
{
	template <typename T> class esSingleton
	{
	public:

        static T* Ref()
		{
			return _instance;
		}

		esSingleton()
		{
			_instance = static_cast<T*>(this);
		}

		virtual ~esSingleton(){}

	protected:

		static T* _instance;

    private:

        T& operator=(T const&);

	}; // esSingleton

    template<typename T>
    inline T& esSingleton<T>::operator=(T const &)
    {return *_instance;}

} // endless

/*
class SomeClass : public esSingleton<SomeClass>
template<> SomeClass* esSingleton<SomeClass>::_instance = 0;
*/
