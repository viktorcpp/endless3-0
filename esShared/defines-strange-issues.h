#pragma once

#pragma warning(disable:4845)
/*
E1097 unknown attribute "no_init_all"
Info 01.31.2020
https://developercommunity.visualstudio.com/content/problem/387702/vs-2017-1592-reports-unknown-attribute-no-init-all.html
*/

#pragma warning(disable:4290)
/*
C++ exception specification ignored except to indicate a function is not __declspec(nothrow)
Info 01.31.2020
https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-3-c4290?view=vs-2019
*/
