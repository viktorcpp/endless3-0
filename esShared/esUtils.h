#pragma once

#include "defines.h"
#include "includes-std.h"

namespace endless
{
    namespace esUtils
    {
        static int rand( int min, int max )
        {
            std::srand( (ulong)GetTickCount64() );
            return min + std::rand() % (max - min);

        } // rand

        template<typename ... Args> static std::string string_format( const char* format, Args ... args )
        {
            size_t size = snprintf( nullptr, 0, format, args ... ) + 1; // Extra space for '\0'
            std::unique_ptr<char[]> buf( new char[ size ] );
            snprintf( buf.get(), size, format, args ... );
            return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
        }

        static std::string& TranslateLastError( std::string& out )
	    { 
		    LPTSTR msgbuff = 0;
		    ulong  dword   = GetLastError();
            
		    FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM, NULL, dword, MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), (LPTSTR)&msgbuff, 0, NULL );
		    
            out = string_format( "%d: %s", dword, msgbuff );

            LocalFree(msgbuff);

            return out;

	    } // TranslateLastError

        /* Convert Multi-Bite to Wide */
        static void LtoW( const char* in, std::wstring& out )
        {
            std::string str_pre(in);
            out = std::wstring( str_pre.begin(), str_pre.end() );

        } // LtoW

        /* Convert Wide to Multi-Bite */
        static void WtoL( const wchar_t* in, std::string& out )
        {
            std::wstring str_pre(in);
            out = std::string( str_pre.begin(), str_pre.end() );

        } // WtoL

        static std::string FormatDateTime( std::string& out )
        {
            struct tm _ptime;
            std::time_t _ctime;
            std::time(&_ctime);
            localtime_s( &_ptime, &_ctime );

            out = string_format( "%02d.%02d.%04d %02d:%02d:%02d",
                                _ptime.tm_mon  + 1,
                                _ptime.tm_mday,
                                _ptime.tm_year + 1900,
                                _ptime.tm_hour,
                                _ptime.tm_min,
                                _ptime.tm_sec );
            return out;

        } // FormatDateTime

        static std::string FormatDate( std::string& out )
        {
            struct tm _ptime;
            std::time_t _ctime;
            std::time(&_ctime);
            localtime_s( &_ptime, &_ctime );

            out = string_format( "%02d.%02d.%04d",
                                _ptime.tm_mon  + 1,
                                _ptime.tm_mday,
                                _ptime.tm_year + 1900 );
            return out;

        } // FormatDate

        static std::string FormatTime( std::string& out )
        {
            struct tm _ptime;
            std::time_t _ctime;
            std::time(&_ctime);
            localtime_s( &_ptime, &_ctime );

            out = string_format( "%02d.%02d.%02d",
                                _ptime.tm_hour,
                                _ptime.tm_min,
                                _ptime.tm_sec );
            return out;

        } // FormatTime

        static int alert( const char* msg, const char* title = "alert" )
        {
            return MessageBox( NULL, msg, title, MB_OK );

        } // alert

        static int alert( std::string msg, std::string title = "alert" )
        {
            return MessageBox( NULL, msg.c_str(), title.c_str(), MB_OK );

        } // alert

        /* преобразует градусы в радианы */
		static inline float FOVDegreeToRadian(float degree)
		{
			//return (float)( degree*(XM_PI / 180.0f) );
		}

		/* преобразует радианы в градусы (float)1->90 */
		static inline float FOVRadianToDegree(float radian)
		{
			//return (float)(radian*(180.0f / XM_PI));
		}

        /*static float XMVector3Distance( const XMVECTOR& veca, const XMVECTOR& vecb )
        {
            XMFLOAT3 ponta;
            XMFLOAT3 pontb;
            XMStoreFloat3( &ponta, veca );
            XMStoreFloat3( &pontb, vecb );

            return sqrt( (ponta.x - pontb.x) * (ponta.x - pontb.x) +
                         (ponta.y - pontb.y) * (ponta.y - pontb.y) +
                         (ponta.z - pontb.z) * (ponta.z - pontb.z) );

        } // XMVector3Distance*/

        /*
        static HRESULT CompileShaderFromFile( const char* filename, const char* entry_name, const char* version, ID3DBlob** blob_out )
        {
            HRESULT      hr           = S_OK;
            ID3DBlob*    blob_error   = nullptr;
            std::wstring wstr         = L"";
            DWORD        shader_flags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
            shader_flags |= D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
            MUtils::LtoW( filename, wstr );

            hr = D3DCompileFromFile( wstr.c_str(), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, entry_name, version, shader_flags, 0, blob_out, &blob_error );
            
            if( FAILED(hr) )
            {
                if( blob_error )
                {
                    OutputDebugStringA( reinterpret_cast<const char*>( blob_error->GetBufferPointer() ) );
                    blob_error->Release();
                }
                return hr;
            }
            if( blob_error ) blob_error->Release();

            return S_OK;

        } // CompileShaderFromFile
        */

        /*
        returns path like - "C:/program/current/data/file.txt"
        @param - string like - data/file.txt or file.txt
        */
        static std::string GetFullPath( std::string path_add )
        {
            std::string p = fs::current_path().u8string() + "/";
            std::replace( p.begin(), p.end(), '\\', '/' );

            p += path_add;

            return p;

        } // GetFullPath

        /*
        returns path like - "C:/program/current/"
        */
        static std::string GetCurDir()
        {
            std::string p = fs::current_path().u8string();
            std::replace( p.begin(), p.end(), '\\', '/' );
            
            return std::string(p);

        } // GetCurDir

        static bool FileExists( const char* path )
        {
            return fs::exists( path );
            
        } // FileExists

        static bool FileLoadBuffer( const char* path, std::stringstream& buffer )
        {
            if( !fs::exists( path ) ) return false;

            std::ifstream file( path );
            buffer << file.rdbuf();
            file.close();

            return true;

        } // FileLoadBuffer

        static bool FileLoadBufferW( const char* path, std::wstringstream& buffer )
        {
            if( !fs::exists( path ) ) return false;

            std::ifstream file( path );
            buffer << file.rdbuf();
            file.close();

            return true;

        } // FileLoadBufferW

        static const ulong FileSize( const char* path )
        {
            return (ulong)fs::file_size(path);

        } // FileSize

        static void split( const std::string& s, char delimiter, std::vector<std::string>& out )
        {
            std::string        token;
            std::istringstream token_stream(s);

            while( std::getline(token_stream, token, delimiter) )
                out.push_back(token);

        } // split

        static std::string ltrim( const std::string& str )
        {
            return str.substr( str.find_first_not_of( " \f\n\r\t\v" ) );
        }

        static std::string rtrim( const std::string& str )
        {
            return str.substr( 0, str.find_last_not_of( " \f\n\r\t\v" ) + 1 );
        }

        static std::string trim( const std::string& str )
        {
            return ltrim( rtrim( str ) );
        }

    } // namespace utils

} // namespace endless
