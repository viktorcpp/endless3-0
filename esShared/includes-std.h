#pragma once

#include <windows.h>
#include <combaseapi.h>
#include <memory>
#include <time.h>

#include <string>
#include <experimental/filesystem>
//#include <filesystem> // C++17 for vs2019 if...
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <functional>
#include <map>
#include <unordered_map>
#include <chrono>

namespace fs = std::experimental::filesystem;
