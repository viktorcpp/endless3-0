#pragma once

#include "defines.h"
#include "includes-std.h"

#ifdef ES_SHARED_EXPORT
#define ES_SHARED_API __declspec(dllexport)
#else
#define ES_SHARED_API __declspec(dllimport)
#endif

#include "esSingleton.h"
#include "esUtils.h"

namespace endless
{
    namespace esshared
    {
        //

    } // namespace esshared

} // namespace endless

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved );
