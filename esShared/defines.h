#pragma once

#include "defines-strange-issues.h"

#define WIN32_LEAN_AND_MEAN // Exclude rarely-used stuff from Windows headers

#define __TRY__ try{
#define __CATCH__ } catch( std::exception exc ) {\
std::string _what    ( exc.what() );     /* Bad argument exception in */\
std::string _function( __FUNCTION__ ); /* MCore::Setup */ \
std::string _file    ( __FILE__ );     /* File: MCore.cpp */ \
std::string _line    ( std::to_string( __LINE__ ) );     /* Line: 144 */ \
std::string _nl      ( "\n" ); \
std::string _full( _what + _nl + _function + _nl + _file + _nl + _line ); \
throw std::exception( _full.c_str() );\
}

#ifdef INTERFACE
#undef INTERFACE
#endif
#define INTERFACE

#ifdef ABSTRACT
#undef ABSTRACT
#endif
#define ABSTRACT

#ifdef OVERRIDE
#undef OVERRIDE
#endif
#define OVERRIDE

#ifndef WM_MOUSEWHEEL
#	define WM_MOUSEWHEEL 0x020A
#endif

#define stringify( val ) #val"\0"

typedef unsigned __int8    uint8;
typedef unsigned __int16   uint16;
typedef unsigned __int32   uint32;
typedef unsigned __int64   uint64;
typedef unsigned   long    ulong;
typedef unsigned   short   ushort;

typedef signed   __int8  int8;
typedef signed   __int16 int16;
typedef signed   __int32 int32;
typedef signed   __int64 int64;

template<typename T> void __delete (T e){ if(e != 0){ delete    e; } e = 0; }
template<typename T> void __deletea(T e){ if(e != 0){ delete[]  e; } e = 0; }
template<typename T> void __release(T e){ if(e != 0){ e->release(); } e = 0; }
template<typename T> void __Release(T e){ if(e != 0){ e->Release(); } e = 0; }
template<typename T> void __destroy(T e){ if(e != 0){ e->destroy(); } e = 0; }
