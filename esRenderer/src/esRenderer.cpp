
#include "esRenderer.h"

namespace endless
{
    namespace esrenderer
    {

        OVERRIDE void esRenderer::DrawFrame(HWND hwnd)
        {
            //

        } // DrawFrame

        OVERRIDE void esRenderer::GetMonitorsNum(int& out)
        {
            out = ::GetSystemMetrics(SM_CMONITORS);

        } // GetMonitorsNum

        OVERRIDE void esRenderer::GetMonitorSize(int& w, int& h)
        {
            w = ::GetSystemMetrics(SM_CXSCREEN);
            h = ::GetSystemMetrics(SM_CYSCREEN);

        } // GetMonitorSize

        OVERRIDE void esRenderer::GetMonitorResList(std::vector<esRendererAPI::MonResMode>& res_modes)
        {
            DISPLAY_DEVICE dd;
            dd.cb = sizeof(DISPLAY_DEVICE);
            EnumDisplayDevicesA( NULL, 0, &dd, NULL );

            DEVMODE dm = { 0 };
            dm.dmSize = sizeof(dm);
            for( int iModeNum = 0; EnumDisplaySettings( dd.DeviceName, iModeNum, &dm ) != 0; iModeNum++ )
            {
                if( dm.dmDisplayFrequency >= 60 && dm.dmPelsWidth >= 1280 )
                    res_modes.push_back( {dm.dmPelsWidth, dm.dmPelsHeight, dm.dmDisplayFrequency} );
            }

        } // GetMonitorResList

        OVERRIDE void esRenderer::GetScreenFullSize(int& w, int& h)
        {
            w = ::GetSystemMetrics(SM_CXVIRTUALSCREEN);
            h = ::GetSystemMetrics(SM_CYVIRTUALSCREEN);

        } // GetScreenFullSize

        OVERRIDE void esRenderer::Setup(HWND hwnd, int w, int h)
        {
            hWnd = hwnd;

        } // Setup

        OVERRIDE esRendererAPI* esRendererAPI::Create()
        {
            return new esRenderer();

        } // Create

        OVERRIDE void esRenderer::Destroy()
        {
            delete this;

        } // Destroy

        esRenderer::esRenderer()
        {
            //
        }

        esRenderer::~esRenderer()
        {
            //
        }

    } // namespace esscripts
    
} // namespace endless
