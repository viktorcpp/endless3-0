#pragma once

#include "esShared/esUtils.h"
#include "esRendererAPI.h"

#ifdef _DEBUG
#pragma comment(lib, "esRendererD3D_d.lib")
#else
#pragma comment(lib, "esRendererD3D.lib")
#endif

namespace endless
{
    namespace esrenderer
    {
        class esRenderer : public esRendererAPI
        {
        public:

            OVERRIDE void DrawFrame(HWND hwnd);

            OVERRIDE void GetMonitorsNum(int& out);
            OVERRIDE void GetMonitorSize(int& w, int& h);
            OVERRIDE void GetMonitorResList(std::vector<MonResMode>&);
            OVERRIDE void GetScreenFullSize(int&,int&);

            /*
            HWND - main window handle
            int  - main window width
            int  - main window height
            */
            OVERRIDE void Setup(HWND, int, int);
            OVERRIDE void Destroy();

        private:

            HWND hWnd;
            int width  = 0;
            int height = 0;
            std::string error = "";

            esRenderer();
            virtual ~esRenderer();

        friend class esRendererAPI;

        }; // class esRenderer

    } // namespace esscripts

} // namespace endless
