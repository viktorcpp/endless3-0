#pragma once

#include "esShared/defines.h"
#include "esShared/includes-std.h"

#ifdef ES_RENDERER_EXPORT
#define ES_RENDERER_API __declspec(dllexport)
#else
#define ES_RENDERER_API __declspec(dllimport)
#endif

namespace endless
{
    namespace esrenderer
    {
        INTERFACE class ES_RENDERER_API esRendererAPI
        {
        public:

            typedef enum
            {
                RENDERER_NULL = 0,
                RENDERER_D3D  = 1

            } RENDERER_API;

            typedef struct
            {
                uint32 width;
                uint32 height;
                uint32 freq;

            } MonResMode;

            virtual void DrawFrame(HWND) = 0;
            virtual void Setup(HWND,int,int) = 0;
            static  esRendererAPI* Create();
            virtual void Destroy() = 0;

            virtual void GetMonitorsNum(int&) = 0;
            virtual void GetMonitorSize(int&,int&) = 0;
            virtual void GetMonitorResList(std::vector<MonResMode>&) = 0;
            virtual void GetScreenFullSize(int&,int&) = 0;

        };

    } // namespace esrenderer

} // namespace endless
