#pragma once

namespace endless
{
    struct
    {
        std::string appname              = "SOME NAME";
        std::string datapath             = "../data/";
        float       window_w             = 1024.f;
        float       window_h             = 768.f;
        bool        window_borderless    = true;
        bool        window_fullscreen    = false;
        bool        render_vsync         = false;
	    int         render_msaa          = 1; // 0-none; 1-16 more powerfull MSAA
	    int         render_msaa_quality  = 1; // can't be more than can your device
	    float       render_znear         = 0.001f;
        float       render_zfar          = 10000.f;
        float       render_fov           = 45.f;
        float       render_cam_rot_speed = 20.f;
        int         render_api           = 1;
        int         snd_max_channels     = 100;
		float       snd_doppler_scale    = 1.f;
		float       snd_distance_factor  = 1.f;
		float       snd_rolloff_scale    = 1.f;
        float       phy_gravity_x        = 0.0f;
        float       phy_gravity_y        = 0.0f;
        float       phy_gravity_z        = 0.0f;
	    float       phy_timestep         = 120.f;

    } CFG;

    class esConfigs : public esSingleton<esConfigs>
    {
    public:

        void Setup() throw(std::exception);

        esConfigs();
        virtual ~esConfigs();

    private:

        esConfigs(esConfigs&) = delete;

    }; // class esConfigs

} // namespace endless
