#pragma once

#include "defines.h"
#include "esShared/defines.h"
#include "esShared/esShared.h"
#include "esShared/includes-std.h"
#include "esShared/esSingleton.h"
#include "esShared/esUtils.h"
#include "esSound/headers/esSoundAPI.h"
#include "esScripts/headers/esScriptsAPI.h"
#include "esInput/headers/esInputAPI.h"
#include "esHUD/headers/esHUDAPI.h"
#include "esPhysics/headers/esPhysicsAPI.h"
#include "esRenderer/headers/esRendererAPI.h"
#include "../resource.h"

#ifdef _DEBUG
#pragma comment(lib, "esSound_d.lib")
#pragma comment(lib, "esScripts_d.lib")
#pragma comment(lib, "esInput_d.lib")
#pragma comment(lib, "esHUD_d.lib")
#pragma comment(lib, "esPhysics_d.lib")
#pragma comment(lib, "esShared_d.lib")
#pragma comment(lib, "esRenderer_d.lib")
#else
#pragma comment(lib, "esSound.lib")
#pragma comment(lib, "esScripts.lib")
#pragma comment(lib, "esInput.lib")
#pragma comment(lib, "esHUD.lib")
#pragma comment(lib, "esPhysics.lib")
#pragma comment(lib, "esShared.lib")
#pragma comment(lib, "esRenderer.lib")
#endif

#include "esTimer.h"
#include "esLogger.h"
#include "esCore.h"
#include "esWindow.h"
#include "esConfigs.h"

#include "esLogger.cpp"
#include "esTimer.cpp"
#include "esWindow.cpp"
#include "esConfigs.cpp"
