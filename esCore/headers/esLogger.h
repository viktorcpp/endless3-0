#pragma once

#include "defines.h"
#include "includes-std.h"

namespace endless
{
    namespace esshared
    {

#define LOGN(...)  esshared::esLogger::Ref()->LogN (__VA_ARGS__);
#define LOGW(...)  esshared::esLogger::Ref()->LogW (__VA_ARGS__);
#define LOGE(...)  esshared::esLogger::Ref()->LogE (__VA_ARGS__);
#define LOGF(...)  esshared::esLogger::Ref()->LogF (__VA_ARGS__);

        class esLogger : public esSingleton<esLogger>
        {
        public:

            template<typename ... Args> esLogger* LogN( const char* format, Args ... args );
            template<typename ... Args> esLogger* LogW( const char* format, Args ... args );
            template<typename ... Args> esLogger* LogE( const char* format, Args ... args );
            template<typename ... Args> esLogger* LogF( const char* format, Args ... args );

            esLogger();
            virtual ~esLogger();

        private:

            esLogger(esLogger&) = delete;

        }; // class esLogger

    } // namespace esshared

} // namespace endless
