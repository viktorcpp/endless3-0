#pragma once

namespace endless
{
    using namespace esshared;
    using namespace esshared;
    using namespace essound;
    using namespace esscripts;
    using namespace esinput;
    using namespace eshud;
    using namespace esphysics;
    using namespace esrenderer;

    class esCore : public esSingleton<esCore>
    {
    public:
        
        void Setup() throw(std::exception);
        void Run() throw(std::exception);

        inline HINSTANCE   GetModule() { return module; }

        inline esSoundAPI*    SOUND()      const { return es_sound; }
        inline esScriptsAPI*  SCRIPTS()    const { return es_scripts; }
        inline esInputAPI*    INPUT()      const { return es_input; }
        inline esHUDAPI*      HUD()        const { return es_hud; }
        inline esPhysicsAPI*  PHYSICS()    const { return es_physics; }
        inline esRendererAPI* RENDERER()   const { return es_renderer; }
        inline esWindow*      WINDOW()     const { return es_window; }
        inline esLogger*      LOGGER()     const { return es_logger; }
        inline esConfigs*     CONFIGS()    const { return es_configs; }
        inline esTimer*       TIMER_GLOB() const { return es_timer; }

        // do exit throught THIS func
        bool Destroyed( bool destroy = false );

        static LRESULT CALLBACK MsgPump(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
        static void    ScriptsFatalHandle( void* udata, const char* msg );
        static void    SoundsFatalHandle( const char* msg );

        esCore();
        virtual ~esCore();

    private:

        HINSTANCE module       = nullptr;
        bool      is_destroyed = false;

        esSoundAPI*    es_sound    = nullptr;
        esScriptsAPI*  es_scripts  = nullptr;
        esInputAPI*    es_input    = nullptr;
        esHUDAPI*      es_hud      = nullptr;
        esPhysicsAPI*  es_physics  = nullptr;
        esRendererAPI* es_renderer = nullptr;
        esWindow*      es_window   = nullptr;
        esLogger*      es_logger   = nullptr;
        esConfigs*     es_configs  = nullptr;
        esTimer*       es_timer    = nullptr;

        esCore( esCore& ) = delete;

    }; // class esCore

} // namespace endless
