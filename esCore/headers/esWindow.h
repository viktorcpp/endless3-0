#pragma once

namespace endless
{
    class esWindow : public esSingleton<esWindow>
    {
    public:

        inline HWND GetHwnd(){ return hWnd; }
        inline bool IsActive(){ return is_active; }

        void Setup();
        void SetVisible( bool vis = true );
        void MoveCenter(int width, int height);
        void Resize(int w, int h);
        void Activate(bool activate);

    private:

        esWindow();
        ~esWindow();
        esWindow( esWindow& );

        HWND hWnd        = nullptr;
        bool is_centered = false;
        bool is_active   = true;

        friend class esCore;
        
    }; // class esWindow

} // namespace endless
