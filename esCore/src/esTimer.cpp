
namespace endless
{

using namespace std::chrono;

    long long esTimer::DeltaTime()
    {
        return __max( delta_time, 1L );

    } // DeltaTime

    long long esTimer::TimeLocal()
    {
        return duration_cast<std::chrono::microseconds>(time_frame_end - time_run).count();

    } // TimeLocal

    void esTimer::RemTimeRun()
    {
        time_run = high_resolution_clock::now();

    } // RemTimeRun

    void esTimer::RemTimeFrameBegin()
    {
        time_frame_begin = high_resolution_clock::now();

    } // RemTimeFrameBegin

    void esTimer::RemTimeFrameEnd()
    {
        time_frame_end = high_resolution_clock::now();
        delta_time     = duration_cast<std::chrono::microseconds>(time_frame_end - time_frame_begin).count();

    } // RemTimeFrameEnd

    void esTimer::Setup()
    {}

    esTimer::esTimer():
        time_run(),
        time_frame_begin(),
        time_frame_end(),
        delta_time(1L)
    {}

    esTimer::~esTimer()
    {}

} // namespace endless
