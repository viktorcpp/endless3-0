
namespace endless
{
    void esConfigs::Setup() throw(std::exception)
    {
        __TRY__

        using namespace esscripts;

        esScriptsAPI* es_scripts = esCore::Ref()->SCRIPTS();

        LOGN("loading configs...\n");

        es_scripts->DoString("var CFG = {};");
        es_scripts->DoString("var SOUNDS_INDEX = {};");

        try
        {
            if( !esUtils::FileExists("endless.script") )
            {
                throw std::exception(" ... 'endless.script' FAILED: doesn't exists, using defaults\n");
            }

            ESSCRIPTS_ERRORS _ret = es_scripts->DoFile("endless.script");

            es_scripts->GetS("CFG.AppName", CFG.appname);
            es_scripts->GetS("CFG.DataPath", CFG.datapath);
            es_scripts->GetF("CFG.Window.width", CFG.window_w);
            es_scripts->GetF("CFG.Window.height", CFG.window_h);
            es_scripts->GetB("CFG.Window.borderless", CFG.window_borderless);
            es_scripts->GetB("CFG.Window.bFullscreen", CFG.window_fullscreen);
            es_scripts->GetB("CFG.Render.vsync", CFG.render_vsync);
	        es_scripts->GetI("CFG.Render.msaa", (long&)CFG.render_msaa);
	        es_scripts->GetI("CFG.Render.msaa_quality", (long&)CFG.render_msaa_quality);
	        es_scripts->GetF("CFG.Render.znear", CFG.render_znear);
            es_scripts->GetF("CFG.Render.zfar", CFG.render_zfar);
            es_scripts->GetF("CFG.Render.fov", CFG.render_fov);
            es_scripts->GetF("CFG.Render.cam_rot_speed", CFG.render_cam_rot_speed);
            es_scripts->GetI("CFG.Render.API", (long&)CFG.render_api);
            es_scripts->GetI("CFG.Sound.max_channels", (long&)CFG.snd_max_channels);
	        es_scripts->GetF("CFG.Sound.Volume.doppler_scale", CFG.snd_doppler_scale);
	        es_scripts->GetF("CFG.Sound.Volume.distance_factor", CFG.snd_distance_factor);
	        es_scripts->GetF("CFG.Sound.Volume.rolloff_scale", CFG.snd_rolloff_scale);
            es_scripts->GetF("CFG.Physics.gravity.x", CFG.phy_gravity_x);
            es_scripts->GetF("CFG.Physics.gravity.y", CFG.phy_gravity_y);
            es_scripts->GetF("CFG.Physics.gravity.z", CFG.phy_gravity_z);
	        es_scripts->GetF("CFG.Physics.timestep", CFG.phy_timestep);

            LOGN(" ... 'endless.script'\n");
            
        }
        catch(std::exception exc)
        {
            LOGE(exc.what());
        }
        
        std::string path_configs = esUtils::GetFullPath( CFG.datapath + "configs/sounds.script" );
        ESSCRIPTS_ERRORS _ret = es_scripts->DoFile(path_configs.c_str());
        if( _ret != ESSCRIPTS_ERRORS::ESS_OK )
        {
            LOGE(" ... '%s' => esScripts::DoFile FAILED\n ... PATH: '%s'\n", __FUNCTION__, path_configs.c_str());
        }

        std::string str_test  = "123";
        std::string str_test1 = "1234";
        _ret = es_scripts->GetS("SOUNDS_INDEX['snd_test']", str_test);
        
        MessageBox(0, str_test.c_str(), "", MB_OK);

        LOGN("loading configs done\n");

        __CATCH__

    } // Setup

    esConfigs::esConfigs()
    {}

    esConfigs::~esConfigs()
    {}

    template<> esConfigs* esSingleton<esConfigs>::_instance = 0;

} // namespace endless
