
namespace endless
{
    void esWindow::SetVisible( bool vis )
    {
        ShowWindow( hWnd, vis ? SW_SHOW : SW_HIDE );

    } // SetVisible

    void esWindow::MoveCenter( int width, int height )
    {
        RECT rect;
	    GetClientRect(hWnd, &rect);
	    int w = rect.right-rect.left;
	    int h = rect.bottom-rect.top;
	    if(w < width)  width  += width-w;
	    if(h < height) height += height-h;
	    int L = (GetSystemMetrics(SM_CXSCREEN) - width)/2;
	    int T = (GetSystemMetrics(SM_CYSCREEN) - height)/2;
	    MoveWindow(hWnd, L, T, width, height, TRUE);
        is_centered = true;

    } // MoveCenter

    void esWindow::Resize(int w, int h)
    {
        int L = (GetSystemMetrics(SM_CXSCREEN) - w)/2;
        int T = (GetSystemMetrics(SM_CYSCREEN) - h)/2;

        MoveWindow(hWnd, L, T, w, h, TRUE);

    } // Resize

    void esWindow::Activate(bool activate)
    {
        is_active = activate;

        if( is_active )
        {
            SetForegroundWindow(hWnd);
        }

    } // Activate

    void esWindow::Setup()
    {
        __TRY__

        HINSTANCE module = GetModuleHandle(NULL);

        WNDCLASSEX wclex = {};
        wclex.cbSize        = sizeof(WNDCLASSEX);
	    wclex.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	    wclex.hCursor       = LoadCursor(NULL, IDC_ARROW);
	    wclex.hIcon         = LoadIcon( module, MAKEINTRESOURCE(IDI_ICON1) );
	    wclex.hIconSm       = LoadIcon( module, MAKEINTRESOURCE(IDI_ICON1) );
	    wclex.hInstance     = module;
	    wclex.lpfnWndProc   = esCore::MsgPump;
	    wclex.lpszClassName = CFG.appname.c_str();
	    wclex.style         = CS_CLASSDC;

        RegisterClassEx(&wclex);

        hWnd = CreateWindow
        (
            wclex.lpszClassName,
            wclex.lpszClassName,
            WS_VISIBLE|WS_OVERLAPPED|WS_POPUP,//WS_OVERLAPPED,//1 ? WS_VISIBLE|WS_POPUP : WS_VISIBLE|WS_OVERLAPPED,
            0,
            0,
            (int)CFG.window_w,
            (int)CFG.window_h,
            GetDesktopWindow(),
            NULL,
            wclex.hInstance,
            (LPVOID) this
        );
        
        SetWindowLong( hWnd, GWL_STYLE, 0 );
        
        ShowWindow(hWnd, SW_SHOWDEFAULT);
        UpdateWindow(hWnd);

        __CATCH__

    } // Setup

    esWindow::esWindow():
        hWnd(nullptr),
        is_centered(false),
        is_active(true)
    {}

    esWindow::esWindow(esWindow&)
    {}

    esWindow::~esWindow()
    {
        DestroyWindow(hWnd);
        UnregisterClass( "APPNAME", GetModuleHandle(NULL) );
    }

    template<> esWindow* esSingleton<esWindow>::_instance = 0;

} // namespace endless
