
namespace endless
{
    namespace esshared
    {
        // error level consts
        const char* LOGLEVEL_CONSTS[]  =
        {
            "[N]",
            "[W]",
            "[E]",
            "[F]"
        };

        typedef enum _LogLevel
        {
            LOG_NORMAL = 0,
            LOG_WARNING,
            LOG_ERROR,
            LOG_FATAL

        } LOG_LEVEL;

        std::ofstream     log_n;
        std::ofstream     log_e;
        std::ofstream     log_w;
        std::ofstream     log_f;
        std::stringstream hud_buffer;

        // local hidden helper function for Log%%% methods
        template<typename ... Args> inline void esLogger_InternalLogHelper( LOG_LEVEL err_level, std::ofstream& stream, const char* format, Args ... args )
        {
            std::string stime_buffer;
            esUtils::FormatTime(stime_buffer);

            std::string out = esUtils::string_format( "[%s]%s  ", stime_buffer.c_str(), LOGLEVEL_CONSTS[err_level] );

            out += esUtils::string_format( format, args... );

            stream << out.c_str() << std::flush;

            if( err_level != LOG_LEVEL::LOG_NORMAL )
                log_n  << out.c_str() << std::flush;

        } // esLogger_InternalLogHelper

        template<typename ... Args> esLogger* esLogger::LogN( const char* format, Args ... args )
        {
            esLogger_InternalLogHelper( LOG_NORMAL, log_n, format, args... );

            return this;

        } // LogN

        template<typename ... Args> esLogger* esLogger::LogW( const char* format, Args ... args )
        {
            esLogger_InternalLogHelper( LOG_WARNING, log_w, format, args... );

            return this;

        } // LogW

        template<typename ... Args> esLogger* esLogger::LogE( const char* format, Args ... args )
        {
            esLogger_InternalLogHelper( LOG_ERROR, log_e, format, args... );

            return this;

        } // LogE

        template<typename ... Args> esLogger* esLogger::LogF( const char* format, Args ... args )
        {
            esLogger_InternalLogHelper( LOG_FATAL, log_f, format, args... );

            return this;

        } // LogE

        esLogger::esLogger()
        {
            std::experimental::filesystem::create_directory("logs/");
        
            log_n.open("logs/log-normal.txt", std::ios::out);
            log_w.open("logs/log-warn.txt",   std::ios::out);
            log_e.open("logs/log-error.txt",  std::ios::out);
            log_f.open("logs/log-fatal.txt",  std::ios::out);

            std::string date;
            esUtils::FormatDateTime(date);

            log_n << "++++++++++++++++++++++++++++++++ " << date.c_str() << " ++++++++++++++++++++++++++++++++\n";
            log_n << "Logger is ready.\n";
            log_n << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n";

        } // esLogger()

        esLogger::~esLogger()
        {
            log_n.close();
            log_w.close();
            log_e.close();
            log_f.close();

        } // ~esLogger()

    } // namespace esshared

    template<> esshared::esLogger* esSingleton<esshared::esLogger>::_instance = 0;

} // namespace endless
