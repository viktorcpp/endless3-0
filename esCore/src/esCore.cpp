
#include "includes.h"

namespace endless
{
    void esCore::Setup() throw(std::exception)
    {
        __TRY__

        LOGN( "esCore init START\n" );

        es_timer    = new esTimer();
        es_logger   = esLogger::Ref();
        es_configs  = new esConfigs();
        es_scripts  = esScriptsAPI::Create();
        es_sound    = esSoundAPI::Create();
        es_input    = esInputAPI::Create();
        es_window   = new esWindow();
        es_renderer = esRendererAPI::Create();
        es_hud      = esHUDAPI::Create();
        es_physics  = esPhysicsAPI::Create();
        
        {
            es_scripts->Setup();
            LOGN( "esScriptsAPI is READY\n" );
        }

        {
            es_configs->Setup();
            LOGN( "esConfigs is READY\n" );
        }

        es_window->Setup();

        {
            essound::SOUND_OPTIONS sound_options;
            sound_options.max_channels    = CFG.snd_max_channels;
            sound_options.doppler_scale   = CFG.snd_doppler_scale;
            sound_options.distance_factor = CFG.snd_distance_factor;
            sound_options.rolloff_scale   = CFG.snd_rolloff_scale;

            es_sound->Setup(sound_options, esCore::SoundsFatalHandle);

            LOGN( "esSoundAPI is READY\n" );
        }
        es_input   ->Setup( es_window->GetHwnd() );
        es_renderer->Setup( es_window->GetHwnd(), (int)CFG.window_w, (int)CFG.window_h );
        es_hud     ->Setup( es_window->GetHwnd() );
        es_physics ->Setup();

        LOGN( "esCore init DONE\n" );

        __CATCH__

    } // Setup

    void esCore::Run() throw(std::exception)
    {
        __TRY__

        esCore::TIMER_GLOB()->RemTimeRun();

        esCore*   es_core = esCore::Ref();
        esWindow* window  = esWindow::Ref();
        HWND      hWnd    = window->GetHwnd();
        MSG       msg;

        ZeroMemory( &msg, sizeof(msg) );

        while( !is_destroyed )
        {
            if( window->IsActive() )
            {
                if( PeekMessage( &msg, NULL, NULL, NULL, PM_REMOVE ) )
                {
                    TranslateMessage(&msg);
                    DispatchMessage(&msg);
                }

                esCore::TIMER_GLOB()->RemTimeFrameBegin();

                esCore::INPUT()   ->Update();
                esCore::PHYSICS() ->Update();
                esCore::RENDERER()->DrawFrame(hWnd);
                esCore::SOUND()   ->Update();
                esCore::INPUT()   ->Reset();

                esCore::TIMER_GLOB()->RemTimeFrameEnd();
            }
            else
            {
                if( GetMessage( &msg, NULL, 0, 0 ) )
                {
                    TranslateMessage(&msg);
                    DispatchMessage(&msg);
                }
            }

        } // while

        __CATCH__

    } // Run

    LRESULT CALLBACK esCore::MsgPump(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
    {
        esCore*     es_core      = esCore::Ref();
        esInputAPI* es_input_api = es_core->INPUT();
        esWindow*   es_window    = esWindow::Ref();
        RECT        rect;

        GetWindowRect(hWnd, &rect);

        es_input_api->ResetUpKeys();

        //SCITER +
        if( message != WM_CREATE )
        {
            BOOL handled = FALSE;
            //LRESULT lr = SciterProcND( hWnd, message, wParam, lParam, &handled );
            //if( handled )
            //    return lr;
        }
        //SCITER -
        
        switch(message)
        {
        case WM_INPUT:
            es_input_api->ReadM( lParam );
            break;

        case WM_SETCURSOR:
            {
                SetCursor( LoadCursor(NULL, IDC_ARROW) );
                return TRUE;
            }
            break;

        case WM_MOUSEWHEEL:
            break;

        case WM_LBUTTONDOWN:
        case WM_LBUTTONUP:
        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
            ::SetCapture(hWnd);
            ::SetForegroundWindow(hWnd);
            break;

        case WM_CHAR:
            break;

        case WM_CREATE:
            SetForegroundWindow(hWnd);
            break;

        case WM_ACTIVATE:
            {
                bool activate = !wParam == WA_INACTIVE;
                esWindow::Ref()->Activate( activate );
                if( activate )
                {
                    SetForegroundWindow(hWnd);
                }
            }
            break;

        // TODO: remove
        case WM_KEYDOWN:
            if( wParam == VK_ESCAPE )
            {
                es_core->Destroyed( true );
            }
            break;

        // CLOSE => DESTROY
        // CLOSE - init Exit
        case WM_CLOSE:
            {
                es_window->Activate(false);
                ::ClipCursor(0);
                DestroyWindow(hWnd);
            }
            break;

        // DESTROY - after window deleting
        case WM_DESTROY:
            es_core->Destroyed( true );
            break;

        default:
            break;

        } // switch

        return DefWindowProc(hWnd, message, wParam, lParam);

    } // MsgPump

    void esCore::ScriptsFatalHandle( void* udata, const char* msg )
    {
        LOGN( "esScripts core FATAL error\n               %s", msg );
        LOGF( "esScripts core FATAL error\n               %s", msg );

    } // ScriptsFatalHandle

    void esCore::SoundsFatalHandle( const char* msg )
    {
        LOGF( "esSound core FATAL error\n               %s", msg );

    } // SoundsFatalHandle

    bool esCore::Destroyed( bool destroy )
    {
        is_destroyed = destroy;

        return is_destroyed;

    } // Destroy

    esCore::esCore()
    {
        module = GetModuleHandle(NULL);

    } // esCore

    esCore::~esCore()
    {
        es_scripts->Destroy();

    } // ~esCore

    template<> esCore* esSingleton<esCore>::_instance = 0;

} // namespace endless

int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
    using namespace endless;
    //SCITER+
    // Sciter needs it for Drag-n-drop, etc.
    OleInitialize(NULL);
    //SCITER-

    try
    {
        esCore es_core;
        es_core.Setup();
        es_core.Run();
        
    }
    catch( std::exception exc )
    {
        MessageBox( 0, exc.what(), "EXCEPTION", MB_OK );
    }

    //MessageBox( 0, "exit", "", MB_OK );
    return 0;

} // WinMain

static endless::esLogger logger_glob;
