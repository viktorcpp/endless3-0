
CFG.AppName  = "SOME NAME";
CFG.DataPath = "../datapack/";

CFG.Window =
{
    width:       1920,
    height:      1020,
    borderless:  true,
    bFullscreen: false
};

CFG.Render =
{
    vsync: false,
    msaa: 1, // 0-none; 1-16 more powerfull MSAA
    msaa_quality: 1, // can't be more than can your device
    znear: 0.001,
    zfar: 10000,
    fov: 45,
    cam_rot_speed: 20,
    API: 1 // 0-null, 1-Direct3D
}

CFG.Sound =
{
    max_channels: 100,
    Volume:
    {
        doppler_scale: 1,
        distance_factor: 1,
        rolloff_scale: 1
    }
}

CFG.Physics =
{
    gravity: { x:0.0, y:-98.1, z:0.0 },
    timestep: 120
}
