
#include "esScripts.h"

namespace endless
{
    namespace esscripts
    {
        OVERRIDE ESSCRIPTS_ERRORS esScripts::GetI(_In_ const char* name, _Out_ long& var)
        {
            ESSCRIPTS_ERRORS js_error_code = ESSCRIPTS_ERRORS::ESS_OK;
            
            std::string result;
            js_error_code = GetProperty(name, result);
            var = atol( result.c_str() );

            return js_error_code;

        } // GetI

        OVERRIDE ESSCRIPTS_ERRORS esScripts::GetD(const char* name, _Out_ double& var)
        {
            ESSCRIPTS_ERRORS js_error_code = ESSCRIPTS_ERRORS::ESS_OK;
            
            std::string result;
            js_error_code = GetProperty(name, result);
            var = atof( result.c_str() );

            return js_error_code;

        } // GetD

        OVERRIDE ESSCRIPTS_ERRORS esScripts::GetF(const char* name, _Out_ float& var)
        {
            ESSCRIPTS_ERRORS js_error_code = ESSCRIPTS_ERRORS::ESS_OK;
            
            std::string result;
            js_error_code = GetProperty(name, result);
            var = (float)atof( result.c_str() );
            
            return js_error_code;

        } // GetF

        OVERRIDE ESSCRIPTS_ERRORS esScripts::GetC(const char* name, _Out_ char& var)
        {
            ESSCRIPTS_ERRORS js_error_code = ESSCRIPTS_ERRORS::ESS_OK;
            
            std::string result;
            js_error_code = GetProperty(name, result);
            var = result.c_str()[0];

            return js_error_code;

        } // GetC

        OVERRIDE ESSCRIPTS_ERRORS esScripts::GetB(const char* name, _Out_ bool& var)
        {
            ESSCRIPTS_ERRORS js_error_code = ESSCRIPTS_ERRORS::ESS_OK;
            
            std::string result;
            js_error_code = GetProperty(name, result);
            var = atoi( result.c_str() ) == 1;

            return js_error_code;

        } // GetB

        OVERRIDE ESSCRIPTS_ERRORS esScripts::GetS(const char* name, _Out_ std::string& var)
        {
            return GetProperty(name, var);

        } // GetS

        ESSCRIPTS_ERRORS esScripts::GetProperty(_In_ const char* prop, _Out_ std::string& var)
        {
            std::vector<const char*> args;
            args.push_back(prop);

            return this->CallFunc("GetProperty", args, var);

        } // GetProperty

        OVERRIDE ESSCRIPTS_ERRORS esScripts::DoString(_In_ std::string& str, const char** out)
        {
            return DoString(str.c_str(), out);

        } // DoString

        OVERRIDE ESSCRIPTS_ERRORS esScripts::DoString(_In_ const char* script_string, const char** out)
        {
            JsErrorCode js_error_code = JsNoError;
            JsValueRef  script_result = nullptr;

            std::string  script_string_mb(script_string);
            std::wstring script_string_wide(script_string_mb.begin(), script_string_mb.end());

            js_error_code = JsRunScript(script_string_wide.c_str(), curr_source_context++, L"", &script_result);
            if( js_error_code != JsNoError )
            {
                return ESSCRIPTS_ERRORS::ESS_ERROR;
            };

            if( out != nullptr )
            {
                // Convert script result to String in JavaScript; redundant if your script returns a String
	            JsValueRef result_js_string;
	            JsConvertValueToString(script_result, &result_js_string);

	            const wchar_t* result_w   = nullptr;
	            size_t         string_len = 0;
	            JsStringToPointer(result_js_string, &result_w, &string_len);
                std::wstring wstr(result_w);
                std::string  str( wstr.begin(), wstr.end() );
                *out = str.c_str();
            }

            return ESSCRIPTS_ERRORS::ESS_OK;

        } // DoString

        OVERRIDE ESSCRIPTS_ERRORS esScripts::DoFile(_In_ std::string& path_filename)
        {
            return DoFile(path_filename.c_str());

        } // DoFile

        OVERRIDE ESSCRIPTS_ERRORS esScripts::DoFile(_In_ const char* path_filename)
        {
            ESSCRIPTS_ERRORS es_error_code = ESSCRIPTS_ERRORS::ESS_OK;

            if( !esUtils::FileExists(path_filename) ) return ESSCRIPTS_ERRORS::ESS_ERROR;

            std::stringstream script;
            esUtils::FileLoadBuffer(path_filename, script);

            es_error_code = this->DoString(script.str().c_str());
            if( es_error_code != ESSCRIPTS_ERRORS::ESS_OK ) return ESSCRIPTS_ERRORS::ESS_ERROR;

            return ESSCRIPTS_ERRORS::ESS_OK;

        } // DoFile

        OVERRIDE ESSCRIPTS_ERRORS esScripts::CallFunc(_In_ const char* func_name, _In_ std::vector<const char*>& vargs, _Out_ std::string& result)
        {
            JsErrorCode     error_code         = JsNoError;
            JsPropertyIdRef function_id_ref    = nullptr;
            JsValueRef      function_value_ref = nullptr;
            JsValueRef      function_result    = nullptr;
            JsValueRef*     args               = new JsValueRef[vargs.size()+1]; // [0] == undefined
            JsValueRef      js_undefined       = nullptr;

            error_code = JsGetUndefinedValue(&js_undefined);

            args[0] = js_undefined;

            for( size_t x = 0; x < vargs.size(); x++ )
            {
                std::string  _arg = vargs[x];
                std::wstring _warg(_arg.begin(), _arg.end());
                JsValueRef   _str_arg_value;
                error_code = JsPointerToString( _warg.c_str(), _warg.size(), &_str_arg_value);
                args[x+1] = _str_arg_value;
            }

            std::string  _func_name = func_name;
            std::wstring _func_wname(_func_name.begin(), _func_name.end());

            error_code = JsGetPropertyIdFromName(_func_wname.c_str(), &function_id_ref);
            if( error_code != JsNoError )
            {
                return ESSCRIPTS_ERRORS::ESS_ERROR;
            }
            error_code = JsGetProperty(curr_global_object, function_id_ref, &function_value_ref);
            if( error_code != JsNoError )
            {
                return ESSCRIPTS_ERRORS::ESS_ERROR;
            }
            error_code = JsCallFunction( function_value_ref, args, 2, &function_result);
            if( error_code != JsNoError )
            {
                return ESSCRIPTS_ERRORS::ESS_ERROR;
            }

            JsValueRef result_js_string;
	        error_code = JsConvertValueToString(function_result, &result_js_string);
            if( error_code != JsNoError )
            {
                return ESSCRIPTS_ERRORS::ESS_ERROR;
            }

            const wchar_t* wstr_result     = nullptr;
            size_t         wstr_result_len = 0;
            error_code = JsStringToPointer(result_js_string, &wstr_result, &wstr_result_len);
            if( error_code != JsNoError )
            {
                return ESSCRIPTS_ERRORS::ESS_ERROR;
            }
            std::wstring str_out(wstr_result);

            result = std::string( str_out.begin(), str_out.end() );

            return ESSCRIPTS_ERRORS::ESS_OK;

        } // CallFunc

        esScriptsAPI* esScriptsAPI::Create()
        {
            return new esScripts();

        } // Create

        OVERRIDE void esScripts::Destroy()
        {
            delete this;

        } // Destroy

        OVERRIDE ESSCRIPTS_ERRORS esScripts::Setup()
        {
            JsErrorCode      js_error_code = JsNoError;
            ESSCRIPTS_ERRORS es_error_code = ESSCRIPTS_ERRORS::ESS_OK;

	        js_error_code = JsCreateRuntime( (JsRuntimeAttributes)(JsRuntimeAttributeEnableExperimentalFeatures), nullptr, &runtime);
            if( js_error_code != JsNoError ) return ESSCRIPTS_ERRORS::ESS_ERROR;
            
	        js_error_code = JsCreateContext(runtime, &context);
            if( js_error_code != JsNoError ) return ESSCRIPTS_ERRORS::ESS_ERROR;

	        js_error_code = JsSetCurrentContext(context);
            if( js_error_code != JsNoError ) return ESSCRIPTS_ERRORS::ESS_ERROR;
            
            js_error_code = JsGetGlobalObject(&curr_global_object);
            if( js_error_code != JsNoError ) return ESSCRIPTS_ERRORS::ESS_ERROR;

            es_error_code = this->RegisterInternalFunctions();
            if( es_error_code != ESSCRIPTS_ERRORS::ESS_OK ) return ESSCRIPTS_ERRORS::ESS_ERROR;

            JsRunScript( L"function GetProperty(p){ return eval(p); }", curr_source_context++, L"", NULL );

            return ESSCRIPTS_ERRORS::ESS_OK;

        } // Setup

        OVERRIDE ESSCRIPTS_ERRORS esScripts::Update()
        {
            ESSCRIPTS_ERRORS es_error_code = ESSCRIPTS_ERRORS::ESS_OK;
            //
            return ESSCRIPTS_ERRORS::ESS_OK;

        } // Update

        OVERRIDE ESSCRIPTS_ERRORS esScripts::TranslateError(_Out_ std::string& error)
        {
            JsErrorCode js_error_code = JsNoError;

            JsValueRef exception;
	        js_error_code = JsGetAndClearException(&exception);
            if( js_error_code == JsErrorInvalidArgument )
            {
                return ESSCRIPTS_ERRORS::ESS_NO_ERROR_SORRY;
            }

            JsPropertyIdRef msg_name;
	        JsGetPropertyIdFromName(L"message", &msg_name);

            JsValueRef msg_value;
	        JsGetProperty(exception, msg_name, &msg_value);

            const wchar_t* message = nullptr;
	        size_t         length = 0;
	        JsStringToPointer(msg_value, &message, &length);

            std::wstring wmessage(message);
            error = std::string(wmessage.begin(), wmessage.end());

            return ESSCRIPTS_ERRORS::ESS_OK;

        } // TranslateError

        ESSCRIPTS_ERRORS esScripts::RegisterInternalFunctions()
        {
            ESSCRIPTS_ERRORS js_error_code = ESSCRIPTS_ERRORS::ESS_OK;

            js_error_code = this->RegisterInternal___ConsoleLog();
            if( js_error_code != ESSCRIPTS_ERRORS::ESS_OK ) return ESSCRIPTS_ERRORS::ESS_ERROR;

            return ESSCRIPTS_ERRORS::ESS_OK;

        } // RegisterInternalFunctions

        ESSCRIPTS_ERRORS esScripts::RegisterInternal___ConsoleLog()
        {
            // Reg console.log(string)
            JsValueRef      console         = nullptr;
            JsValueRef      log_func        = nullptr;
            JsValueRef      global          = nullptr;
            JsPropertyIdRef console_prop_id = nullptr;
            JsPropertyIdRef log_prop_id     = nullptr;
            const char*     consoleString   = "console";
            const char*     log_string      = "log";

            JsCreateObject(&console);
            JsCreateFunction(esScripts::InternalFunc___ConsoleLog, nullptr, &log_func);
            JsCreatePropertyId(log_string, strlen(log_string), &log_prop_id);
            JsSetProperty(console, log_prop_id, log_func, true);
            JsGetGlobalObject(&global);
            JsCreatePropertyId(consoleString, strlen(consoleString), &console_prop_id);
            JsSetProperty(global, console_prop_id, console, true);

            return ESSCRIPTS_ERRORS::ESS_OK;

        } // RegisterInternal___ConsoleLog

        JsValueRef CALLBACK esScripts::InternalFunc___ConsoleLog(JsValueRef callee, bool isConstructCall, JsValueRef* arguments, unsigned short arguments_count, void* callbackState)
        {
            std::stringstream out;
            for( unsigned int index = 1; index < arguments_count; index++ )
            {
                if( index > 1 ) std::cout << "    ";

                JsValueRef     string_value_ref = nullptr;
                const wchar_t* result_wc        = nullptr;
	            size_t         string_length    = 0;

                JsConvertValueToString( arguments[index], &string_value_ref );
	            JsStringToPointer( string_value_ref, &result_wc, &string_length );

                std::wstring result_string(result_wc);
	            out << std::string( result_string.begin(), result_string.end() );
            }

            std::cout << std::endl << out.str() << std::endl;

            return JS_INVALID_REFERENCE;

        } // ConsoleLog

        esScripts::esScripts()
        {}

        esScripts::~esScripts()
        {
	        JsSetCurrentContext(JS_INVALID_REFERENCE);
	        JsDisposeRuntime(runtime);

        } // ~esScripts

    } // namespace esscripts

} // namespace endless

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
    return TRUE;

} // DllMain
