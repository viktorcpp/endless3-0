#pragma once

#include "esShared/defines.h"
#include "esShared/includes-std.h"

#ifdef ES_SCRIPTS_EXPORT
#define ES_SCRIPTS_API __declspec(dllexport)
#else
#define ES_SCRIPTS_API __declspec(dllimport)
#endif

namespace endless
{
    namespace esscripts
    {
        typedef enum _ESSCRIPTS_ERRORS
        {
            ESS_OK,
            ESS_ERROR,
            ESS_NO_ERROR_SORRY

        } ESSCRIPTS_ERRORS;

        INTERFACE class ES_SCRIPTS_API esScriptsAPI
        { 
        public:

            // "CFG['Window']['width']"    OR    "CFG.Window.width"
            virtual ESSCRIPTS_ERRORS GetI(_In_ const char* name, _Out_ long& var) = 0;
            virtual ESSCRIPTS_ERRORS GetD(_In_ const char* name, _Out_ double& var) = 0;
            virtual ESSCRIPTS_ERRORS GetF(_In_ const char* name, _Out_ float& var) = 0;
            virtual ESSCRIPTS_ERRORS GetC(_In_ const char* name, _Out_ char& var) = 0;
            virtual ESSCRIPTS_ERRORS GetB(_In_ const char* name, _Out_ bool& var) = 0;
            virtual ESSCRIPTS_ERRORS GetS(_In_ const char* name, _Out_ std::string& var) = 0;

            virtual ESSCRIPTS_ERRORS DoString(_In_ std::string& str, _Out_ const char** out = nullptr) = 0;
            virtual ESSCRIPTS_ERRORS DoString(_In_ const char* str,  _Out_ const char** out = nullptr) = 0;

            /*
            Two not fatal erros
            Should be handled by externals
            */
            virtual ESSCRIPTS_ERRORS DoFile(std::string& filename) = 0;
            virtual ESSCRIPTS_ERRORS DoFile(const char* filename) = 0;

            virtual ESSCRIPTS_ERRORS CallFunc(_In_ const char* func_name, _In_ std::vector<const char*>& args, _Out_ std::string& result) = 0;

            virtual ESSCRIPTS_ERRORS Setup() = 0;
            virtual ESSCRIPTS_ERRORS Update() = 0;

            virtual ESSCRIPTS_ERRORS TranslateError(_Out_ std::string& error) = 0;

            static esScriptsAPI* Create();
            virtual void Destroy() = 0;

        }; // class esScriptsAPI

    } // namespace esscripts

} // namespace endless
