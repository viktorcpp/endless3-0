#pragma once

#define _JSRT_
#include "chakracorejs/inc/ChakraCore.h"

#ifdef _DEBUG
#pragma comment(lib, "chakracorejs/debug/ChakraCore.lib")
#else
#pragma comment(lib, "chakracorejs/release/ChakraCore.lib")
#endif

#include "esShared/esUtils.h"
#include "esScriptsAPI.h"

namespace endless
{
    namespace esscripts
    {
        class esScripts : public esScriptsAPI
        {
        public:

            OVERRIDE ESSCRIPTS_ERRORS GetI(const char* name, _Out_ long& var);
            OVERRIDE ESSCRIPTS_ERRORS GetD(const char* name, _Out_ double& var);
            OVERRIDE ESSCRIPTS_ERRORS GetF(const char* name, _Out_ float& var);
            OVERRIDE ESSCRIPTS_ERRORS GetC(const char* name, _Out_ char& var);
            OVERRIDE ESSCRIPTS_ERRORS GetB(const char* name, _Out_ bool& var);
            OVERRIDE ESSCRIPTS_ERRORS GetS(const char* name, _Out_ std::string& var);

            // @return - true=ok, false=some error
            OVERRIDE ESSCRIPTS_ERRORS DoString(_In_ std::string& str, _Out_ const char** out = nullptr);
            // @return - true=ok, false=some error
            OVERRIDE ESSCRIPTS_ERRORS DoString(_In_ const char* str, _Out_ const char** out = nullptr);
            // @return - true=ok, false=some error
            OVERRIDE ESSCRIPTS_ERRORS DoFile  (_In_ std::string& path_filename);
            // @return - true=ok, false=some error
            OVERRIDE ESSCRIPTS_ERRORS DoFile  (_In_ const char* path_filename);

            OVERRIDE ESSCRIPTS_ERRORS CallFunc(_In_ const char* func_name, _In_ std::vector<const char*>& args, _Out_ std::string& result);

            /*
            <summary>
                Creates scripting runtime.
                Creates scripting context.
                Sets scripting context as current context.
            </summary> */
            OVERRIDE ESSCRIPTS_ERRORS Setup();
            OVERRIDE ESSCRIPTS_ERRORS Update();

            /*
            <summary>
                If ESS_ERROR is returned, translates internal engine error into a string
            </summary> */
            OVERRIDE ESSCRIPTS_ERRORS TranslateError(_Out_ std::string& error);

            OVERRIDE void Destroy();
            
        private:

            esScripts();
            virtual ~esScripts();

            JsRuntimeHandle runtime             = nullptr;
	        JsContextRef    context             = nullptr;
	        JsValueRef      result              = nullptr;
	        unsigned int    curr_source_context = 0;
            JsValueRef      curr_global_object  = nullptr;

            ESSCRIPTS_ERRORS RegisterInternalFunctions();

            ESSCRIPTS_ERRORS RegisterInternal___ConsoleLog();

            static JsValueRef CALLBACK InternalFunc___ConsoleLog(JsValueRef callee, bool isConstructCall, JsValueRef* arguments, unsigned short arguments_count, void* callbackState);

            ESSCRIPTS_ERRORS GetProperty(_In_ const char* prop, _Out_ std::string& var);

            esScripts( esScripts& ) = delete;

            friend class esScriptsAPI;

        }; // class esScripts

    } // namespace esscripts

} // namespace endless

