#include "esRendererD3D.h"

namespace endless
{
    namespace esrendererd3d
    {
        OVERRIDE bool esRendererD3D::Setup(HWND hwnd, int w,int h)
        {
            hWnd = hwnd;

            HRESULT     hr    = S_OK;
            UINT        feature_levels_count = sizeof(feature_lvls) / sizeof(int);

            this->CreateDevice();
            this->CreateBackBuffer();
            this->CreateRenderTarget();
            this->CreateDepthStencilState();
            this->CreateDepthStencilView();

            context->OMSetRenderTargets( 1, &rendertargetview, depth_view );

            this->CreateViewport();

            return true;

        } // Destroy()

        bool esRendererD3D::CreateDevice()
        {
            HRESULT     hr                   = S_OK;
            std::string error                = "";
            UINT        feature_levels_count = sizeof(feature_lvls) / sizeof(int);

            DXGI_SWAP_CHAIN_DESC dxgi_swap_chain_desc;
            ZeroMemory( &dxgi_swap_chain_desc, sizeof(DXGI_SWAP_CHAIN_DESC) );
            dxgi_swap_chain_desc.BufferCount                        = 1;
            dxgi_swap_chain_desc.BufferDesc.Width                   = (UINT)width;
            dxgi_swap_chain_desc.BufferDesc.Height                  = (UINT)height;
            dxgi_swap_chain_desc.BufferDesc.Format                  = DXGI_FORMAT_R8G8B8A8_UNORM;
            dxgi_swap_chain_desc.BufferDesc.RefreshRate.Numerator   = 60;
            dxgi_swap_chain_desc.BufferDesc.RefreshRate.Denominator = 1;
            dxgi_swap_chain_desc.BufferUsage                        = DXGI_USAGE_RENDER_TARGET_OUTPUT;
            dxgi_swap_chain_desc.OutputWindow                       = hWnd;
            dxgi_swap_chain_desc.SampleDesc.Count                   = 1;
            dxgi_swap_chain_desc.SampleDesc.Quality                 = 0;
            dxgi_swap_chain_desc.Windowed                           = TRUE;

            // get current feature level profile
            hr = D3D11CreateDevice( nullptr,
                                    D3D_DRIVER_TYPE_HARDWARE,
                                    nullptr,
                                    0,
                                    &feature_lvls[0],
                                    feature_levels_count,
                                    D3D11_SDK_VERSION,
                                    NULL,
                                    &feature_lvl_current,
                                    NULL );
            if(FAILED( hr ))
            {
                esUtils::TranslateLastError(error);
                error = esUtils::string_format( "%s : D3D11CreateDevice getting profile FAILED:\n    %s\n", __FUNCTION__, error.c_str() );
                return false;
            }
            // create with current profile
            hr = D3D11CreateDeviceAndSwapChain( nullptr,
                                                D3D_DRIVER_TYPE_HARDWARE,
                                                nullptr,
                                                device_flags,
                                                &feature_lvls[0],
                                                feature_levels_count,
                                                D3D11_SDK_VERSION,
                                                &dxgi_swap_chain_desc,
                                                &swapchain, &device,
                                                &feature_lvl_current,
                                                &context );
            if( FAILED( hr ) )
            {
                esUtils::TranslateLastError(error);
                error = esUtils::string_format( "%s : D3D11CreateDeviceAndSwapChain FAILED:\n    %s\n", __FUNCTION__, error.c_str() );
                return false;
            }

            return true;

        } // CreateDevice()

        bool esRendererD3D::CreateBackBuffer()
        {
            HRESULT hr = S_OK;

            hr = swapchain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (void**)&back_buffer );
            if( FAILED( hr ) )
            {
                esUtils::TranslateLastError(error);
                error = esUtils::string_format( "%s : swapchain->GetBuffer FAILED:\n    %s\n", __FUNCTION__, error.c_str() );
                return false;
            }

            return true;

        } // CreateBackBuffer

        bool esRendererD3D::CreateRenderTarget()
        {
            HRESULT hr = S_OK;

            D3D11_TEXTURE2D_DESC d3d11_texture2d_desc;
	        ZeroMemory( &d3d11_texture2d_desc, sizeof(D3D11_TEXTURE2D_DESC) );
            d3d11_texture2d_desc.Width              = (UINT)width;
            d3d11_texture2d_desc.Height             = (UINT)height;
            d3d11_texture2d_desc.MipLevels          = 1;
            d3d11_texture2d_desc.ArraySize          = 1;
            d3d11_texture2d_desc.Format             = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
            d3d11_texture2d_desc.SampleDesc.Count   = 1;
            d3d11_texture2d_desc.SampleDesc.Quality = 0;
            d3d11_texture2d_desc.Usage              = D3D11_USAGE_DEFAULT;
            d3d11_texture2d_desc.BindFlags          = D3D11_BIND_DEPTH_STENCIL;
            d3d11_texture2d_desc.CPUAccessFlags     = 0;
            d3d11_texture2d_desc.MiscFlags          = 0;

            hr = device->CreateRenderTargetView( back_buffer, NULL, &rendertargetview );
            if( FAILED( hr ) )
            {
                esUtils::TranslateLastError(error);
                error = esUtils::string_format( "%s : CreateRenderTargetView FAILED\n", __FUNCTION__, error.c_str() );
                return false;
            }
        
            hr = device->CreateTexture2D( &d3d11_texture2d_desc, nullptr, &depth_tex );
            if( FAILED( hr ) )
            {
                esUtils::TranslateLastError(error);
                error = esUtils::string_format( "%s : CreateTexture2D FAILED\n", __FUNCTION__, error.c_str() );
                return false;
            }

            return true;

        } // CreateRenderTarget

        bool esRendererD3D::CreateDepthStencilState()
        {
            HRESULT hr = S_OK;

            D3D11_DEPTH_STENCIL_DESC d3d11_depth_stencil_desc;
            ZeroMemory( &d3d11_depth_stencil_desc, sizeof(D3D11_DEPTH_STENCIL_DESC) );
            d3d11_depth_stencil_desc.DepthEnable                  = true;
            d3d11_depth_stencil_desc.DepthWriteMask               = D3D11_DEPTH_WRITE_MASK_ALL;
            d3d11_depth_stencil_desc.DepthFunc                    = D3D11_COMPARISON_LESS;
            d3d11_depth_stencil_desc.StencilEnable                = true;
            d3d11_depth_stencil_desc.StencilReadMask              = 0xFF;
            d3d11_depth_stencil_desc.StencilWriteMask             = 0xFF;
            d3d11_depth_stencil_desc.FrontFace.StencilFailOp      = D3D11_STENCIL_OP_KEEP;
            d3d11_depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
            d3d11_depth_stencil_desc.FrontFace.StencilPassOp      = D3D11_STENCIL_OP_KEEP;
            d3d11_depth_stencil_desc.FrontFace.StencilFunc        = D3D11_COMPARISON_ALWAYS;
            d3d11_depth_stencil_desc.BackFace.StencilFailOp       = D3D11_STENCIL_OP_KEEP;
            d3d11_depth_stencil_desc.BackFace.StencilDepthFailOp  = D3D11_STENCIL_OP_DECR;
            d3d11_depth_stencil_desc.BackFace.StencilPassOp       = D3D11_STENCIL_OP_KEEP;
            d3d11_depth_stencil_desc.BackFace.StencilFunc         = D3D11_COMPARISON_ALWAYS;

            hr = device->CreateDepthStencilState( &d3d11_depth_stencil_desc, &depth_state );
            if( FAILED(hr) )
            {
                esUtils::TranslateLastError(error);
                error = esUtils::string_format( "%s : CreateDepthStencilState FAILED\n", __FUNCTION__, error.c_str() );
                return false;
            }
            context->OMSetDepthStencilState( depth_state, 1 );

            return true;

        } // CreateDepthStencilState

        bool esRendererD3D::CreateDepthStencilView()
        {
            HRESULT hr = S_OK;

            D3D11_DEPTH_STENCIL_VIEW_DESC stencilview_desc;
	        ZeroMemory( &stencilview_desc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC) );
            stencilview_desc.Format             = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
            stencilview_desc.ViewDimension      = D3D11_DSV_DIMENSION_TEXTURE2DMS;
            stencilview_desc.Texture2D.MipSlice = 0;

            hr = device->CreateDepthStencilView( depth_tex, &stencilview_desc, &depth_view );
            if( FAILED( hr ) )
            {
                esUtils::TranslateLastError(error);
                error = esUtils::string_format( "%s : CreateDepthStencilView FAILED\n", __FUNCTION__, error.c_str() );
                return false;
            }

            return true;

        } // CreateDepthStencilView

        bool esRendererD3D::CreateViewport()
        {
            D3D11_VIEWPORT d3d11_viewport;
            ZeroMemory( &d3d11_viewport, sizeof(D3D11_VIEWPORT) );
            d3d11_viewport.Width    = (FLOAT)width;
            d3d11_viewport.Height   = (FLOAT)height;
            d3d11_viewport.MinDepth = 0.0f;
            d3d11_viewport.MaxDepth = 1.0f;
            d3d11_viewport.TopLeftX = 0;
            d3d11_viewport.TopLeftY = 0;

            context->RSSetViewports( 1, &d3d11_viewport );

            return true;

        } // CreateViewport

        OVERRIDE esRendererD3DAPI* esRendererD3DAPI::Create()
        {
            return new esRendererD3D();

        } // Create()

        OVERRIDE void esRendererD3D::Destroy()
        {
            //

        } // Destroy()

        esRendererD3D::esRendererD3D()
        {}

        esRendererD3D::~esRendererD3D()
        {}

    } // namespace esrendererd3d

} // namespace endless