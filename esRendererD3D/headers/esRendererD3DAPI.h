#pragma once

#include "esShared/defines.h"
#include "esShared/includes-std.h"

#ifdef ES_RENDERERD3D_EXPORT
#define ES_RENDERERD3D_API __declspec(dllexport)
#else
#define ES_RENDERERD3D_API __declspec(dllimport)
#endif

namespace endless
{
    namespace esrendererd3d
    {
        INTERFACE class ES_RENDERERD3D_API esRendererD3DAPI
        {
        public:

            virtual bool Setup(HWND,int,int) = 0;
            static  esRendererD3DAPI* Create();
            virtual void Destroy() = 0;
        };

    } // namespace esrenderer

} // namespace endless
