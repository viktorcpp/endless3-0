#include "esShared/esUtils.h"
#include "esRendererD3DAPI.h"
#include "includes.h"

namespace endless
{
    namespace esrendererd3d
    {
        class esRendererD3D : public esRendererD3DAPI
        {
        public:
            
            /*
            HWND - main window handle
            int  - main window width
            int  - main window height
            */
            OVERRIDE bool Setup(HWND, int, int);
            OVERRIDE void Destroy();

        private:

            const D3D_FEATURE_LEVEL feature_lvls[7] =
            {
                D3D_FEATURE_LEVEL_11_1,
                D3D_FEATURE_LEVEL_11_0,
                D3D_FEATURE_LEVEL_10_1,
                D3D_FEATURE_LEVEL_10_0,
                D3D_FEATURE_LEVEL_9_3,
                D3D_FEATURE_LEVEL_9_2,
                D3D_FEATURE_LEVEL_9_1
            };

            HWND hWnd;
            int width  = 0;
            int height = 0;
            std::string error = "";
            D3D_FEATURE_LEVEL        feature_lvl_current = D3D_FEATURE_LEVEL_11_1;
            UINT                     device_flags        = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
            ID3D11Device*            device              = nullptr;
            ID3D11DeviceContext*     context             = nullptr;
            IDXGISwapChain*          swapchain           = nullptr;
            ID3D11RenderTargetView*  rendertargetview    = nullptr;
            ID3D11DepthStencilView*  depth_view          = nullptr;
            ID3D11DepthStencilState* depth_state         = nullptr;
            ID3D11Texture2D*         back_buffer         = nullptr;
            ID3D11Texture2D*         depth_tex           = nullptr;

            bool CreateDevice();
            bool CreateBackBuffer();
            bool CreateRenderTarget();
            bool CreateDepthStencilState();
            bool CreateDepthStencilView();
            bool CreateViewport();

            esRendererD3D();
            virtual ~esRendererD3D();

        friend class esRendererD3DAPI;

        }; // class esRenderer

    } // namespace esscripts

} // namespace endless