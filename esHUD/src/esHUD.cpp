
#include "esHUD.h"

namespace endless
{
    namespace eshud
    {
        void esHUD::Setup( HWND hwnd ) throw(std::exception)
        {
            hWnd = hwnd;

        } // Setup

        esHUDAPI* esHUDAPI::Create()
        {
            return new esHUD();

        } // Create

        esHUD::esHUD()
        {
            //esUtils::alert("Hello from esHUD !");
        }

        esHUD::~esHUD()
        {}

    } // namespace eshud

} // namespace endless

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
    return TRUE;

} // DllMain
