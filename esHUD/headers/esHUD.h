#pragma once

#include "esHUDAPI.h"

namespace endless
{
    namespace eshud
    {
        class ES_HUD_API esHUD : public esHUDAPI
        {
        public:

            OVERRIDE void Setup( HWND hwnd ) throw(std::exception);

            esHUD();
            ~esHUD();

        private:

            HWND hWnd = nullptr;

            esHUD( esHUD& ) = delete;

        }; // class esHUD

    } // namespace eshud

} // namespace endless
