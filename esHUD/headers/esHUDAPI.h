#pragma once

#include "esShared/defines.h"
#include "esShared/includes-std.h"
#include "esShared/esUtils.h"

#ifdef ES_HUD_EXPORT
#define ES_HUD_API __declspec(dllexport)
#else
#define ES_HUD_API __declspec(dllimport)
#endif

namespace endless
{
    namespace eshud
    {
        INTERFACE class ES_HUD_API esHUDAPI
        {
        public:

            static esHUDAPI* Create();

            virtual void Setup( HWND hwnd ) throw(std::exception) = 0;
        
        }; // class esHUDAPI

    } // namespace eshud

} // namespace endless
