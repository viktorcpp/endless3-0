
#include "esSound.h"

namespace endless
{
    namespace essound
    {
        OVERRIDE void esSound::Update()
        {}

        OVERRIDE void esSound::Play( const char* id )
        {}

        OVERRIDE void esSound::PlayUI( const char* id )
        {
            /*
            channel_ui->setPaused(true);
            fmod_system->playSound( FMOD_CHANNEL_FREE, sounds_ui[id].sound, true, &fmod_channel_ui );
            channel_ui->setPaused(false);
            */
        }

        OVERRIDE void esSound::Setup(SOUND_OPTIONS& options, SoundsFatalHandle) throw(std::exception)
        {
            FMOD_ASSERT( FMOD::System_Create(&fmod_system) );
            FMOD_ASSERT( fmod_system->getVersion(&fmod_version) );

            if( fmod_version < FMOD_VERSION )
            {
                std::string error = esUtils::string_format(
                                        "Error! You are using an old version of FMOD %08x. This program requires %08x\n",
                                        fmod_version,
                                        FMOD_VERSION );
                esSound::sounds_fatal_handle( error.c_str() );
                return;
            }

            FMOD_ASSERT( fmod_system->getNumDrivers(&fmod_num_drivers) );

            if( fmod_num_drivers == 0 )
            {
                FMOD_ASSERT( fmod_system->setOutput(FMOD_OUTPUTTYPE_NOSOUND) );
            }
            else
            {
                FMOD_ASSERT( fmod_system->getDriverCaps(0, &fmod_caps, 0, 0, &fmod_speakermode) );
            }

            if( fmod_caps & FMOD_CAPS_HARDWARE_EMULATED )
            {
                /*
                The user has the 'Acceleration' slider set to off!
                This is really bad for latency!
                You might want to warn the user about this.
                */
                FMOD_ASSERT( fmod_system->setDSPBufferSize(1024, 10) );
            }

            fmod_system->getDriverInfo( 0, fmod_driver_name, 256, 0 );

            /*if( strstr(fmod_driver_name, "SigmaTel") )
            {
                
                //Sigmatel sound devices crackle for some reason if the format is PCM 16bit.
                //PCM floating point output seems to solve it.
                
                FMOD_ASSERT( system->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0,0, FMOD_DSP_RESAMPLER_LINEAR) );
            } // */

            fmod_hr = fmod_system->init(options.max_channels, FMOD_INIT_NORMAL, 0);
            if( fmod_hr == FMOD_ERR_OUTPUT_CREATEBUFFER )
            {
                // Ok, the speaker mode selected isn't supported by this soundcard. Switch it back to stereo...
                FMOD_ASSERT( fmod_system->setSpeakerMode(FMOD_SPEAKERMODE_STEREO) );
                //... and re-init.
                FMOD_ASSERT( fmod_system->init(options.max_channels, FMOD_INIT_NORMAL, 0) );
            }

            FMOD_ASSERT( fmod_system->set3DSettings( options.doppler_scale,
                                                     options.distance_factor,
                                                     options.rolloff_scale ) );

        } // Setup

        esSoundAPI* esSoundAPI::Create()
        {
            return new esSound();

        } // Create

        inline bool esSound::FMOD_ASSERT(FMOD_RESULT result)
        {
            if( result != FMOD_OK )
            {
                esSound::sounds_fatal_handle( FMOD_ErrorString(result) );
                return false;
            }

            return true;

        } // FMOD_ASSERT

        esSound::esSound()
        {
            //esUtils::alert("Hello from esSound !");
        }

        esSound::~esSound()
        {}

        SoundsFatalHandle esSound::sounds_fatal_handle = nullptr;

    } // namespace essound

} // namespace endless

BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
    return TRUE;

} // DllMain
