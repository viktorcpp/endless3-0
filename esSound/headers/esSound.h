#pragma once

#include "esSoundAPI.h"

#include <fmod/inc/fmod.hpp>
#include <fmod/inc/fmod_errors.h>

#ifdef ES_SOUND_EXPORT
    #ifdef _DEBUG
        #pragma comment(lib, "fmod/lib/fmodex64_vc.lib")
    #else
        // #pragma comment(lib, "fmodexL64_vc.lib")
        #pragma comment(lib, "fmod/lib/fmodex64_vc.lib")
    #endif
#endif

namespace endless
{
    namespace essound
    {
        typedef struct __SOUND_FILE : public _SOUND_FILE
        {
            FMOD::Sound* sound = nullptr; // for sound controller only

        } SOUND_FILE_IMPL;

        class esSound : public esSoundAPI
        {
        public:

            OVERRIDE void Play  ( const char* id );
            OVERRIDE void PlayUI( const char* id );

            OVERRIDE void Setup(SOUND_OPTIONS&, SoundsFatalHandle) throw(std::exception);

            OVERRIDE void Update();
    
            esSound();
            ~esSound();

        private:

            esSound( esSound& ) = delete;

            static inline bool FMOD_ASSERT(FMOD_RESULT result);

            static SoundsFatalHandle sounds_fatal_handle;

            FMOD_RESULT                                 fmod_hr               = FMOD_OK;
            uint32                                      fmod_version          = 0;
            int                                         fmod_num_drivers      = 0;
            FMOD_CAPS                                   fmod_caps             = 0;
            FMOD_SPEAKERMODE                            fmod_speakermode      = (FMOD_SPEAKERMODE)0;
            char                                        fmod_driver_name[256] = {};
            FMOD::System*                               fmod_system           = nullptr;
            const char*                                 fmod_error            = nullptr;
            FMOD::Channel*                              fmod_channel_ui       = nullptr;
            FMOD::Channel*                              fmod_channel_music    = nullptr;
            std::unordered_map<std::string, SOUND_FILE> sounds_ui;

        }; // class esSound

    } // namespace esSound

} // namespace endless
