#pragma once

#include "esShared/defines.h"
#include "esShared/includes-std.h"
#include "esShared/esUtils.h"
#include "esShared/esSingleton.h"

#ifdef ES_SOUND_EXPORT
#define ES_SOUND_API __declspec(dllexport)
#else
#define ES_SOUND_API __declspec(dllimport)
#endif

namespace endless
{
    namespace essound
    {
        typedef struct _SOUND_OPTIONS
        {
            int   max_channels    = 32;
            float doppler_scale   = 0.0f;
            float distance_factor = 0.0f;
            float rolloff_scale   = 0.0f;

        } SOUND_OPTIONS;

        typedef enum _SOUND_FILE_LOAD_TYPE
        {
            FLT_NULL = 0,
            FLT_STREAM,
            FLT_LOAD

        } SOUND_FILE_LOAD_TYPE;

        typedef enum _SOUND_FILE_TYPE
        {
            SFT_NULL = 0,
            SFT_2D,
            SFT_3D

        } SOUND_FILE_TYPE;

        typedef struct _SOUND_FILE
        {
            long                 id         = -1;
            std::string          id_str     = "";
            std::string          path       = "";
            SOUND_FILE_LOAD_TYPE load_type  = SOUND_FILE_LOAD_TYPE::FLT_NULL;
            SOUND_FILE_TYPE      sound_type = SOUND_FILE_TYPE::SFT_NULL;

        } SOUND_FILE;

        /* see esSound.h
        typedef struct __SOUND_FILE : _SOUND_FILE
        {
            FMOD::Sound* sound = nullptr; // for sound controller only

        } SOUND_FILE_IMPL;
        */

        typedef void(*SoundsFatalHandle)(const char* msg);

        INTERFACE class ES_SOUND_API esSoundAPI
        {
        public:

            virtual void Play  ( const char* id ) = 0;
            virtual void PlayUI( const char* id ) = 0;

            static  esSoundAPI* Create();
            virtual void Setup(SOUND_OPTIONS&, SoundsFatalHandle) throw(std::exception) = 0;
            virtual void Update() = 0;

        }; // class esSoundAPI

    } // namespace essound

} // namespace endless
